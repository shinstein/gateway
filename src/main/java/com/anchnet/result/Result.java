package com.anchnet.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {
    private Integer code;

    private String message;

    public static Result unauthorized() {
        Result result = new Result();
        result.setCode(HttpStatus.UNAUTHORIZED.value());
        result.setMessage("鉴权失败");
        return result;
    }

}
