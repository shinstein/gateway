package com.anchnet.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

@Configuration
public class CustomRouteConfig {
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        /**
         *  配置 /login 和 /register 请求访问 user 微服务
         */
        return builder.routes()
                .route("login", r -> r.path("/login").and().method(HttpMethod.POST).uri("lb://user"))
                .route("register", r -> r.path("/register").and().method(HttpMethod.POST).uri("lb://user"))
                .build();
    }
}
