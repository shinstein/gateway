package com.anchnet.util;

import com.google.gson.Gson;

public class GsonUtil {
    private static final Gson gson = new Gson();

    public static <T> String toJson(T t) {
        return gson.toJson(t);
    }

    public static <T> T fromJson (String json, Class<T> tClass) {
        return gson.fromJson(json, tClass);
    }
}
