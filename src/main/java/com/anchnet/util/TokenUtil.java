package com.anchnet.util;

import com.anchnet.bean.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenUtil {
    // your-secret-key 经过 sha512加密后得到如下key
    private static final String SECRET_KEY = "273f24e8b6eb9ba7fd2b398e635ad1e2ffecf5cf726c1686bc522dd28100803edbd9ffcf560c03d2173589c7b5c0b09eb32bf8f8c58e771d469a3db2c21a65dc";

    public static String generateToken(User user) {
        return Jwts.builder()
                .claim("id", user.getId())
                .claim("name", user.getName())
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public static User parseToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();

        User user = new User();
        user.setId(claims.get("id", Integer.class));
        user.setName(claims.get("name", String.class));

        return user;
    }
}
