package com.anchnet.filter;

import cn.hutool.core.util.StrUtil;
import com.anchnet.bean.User;
import com.anchnet.result.Result;
import com.anchnet.util.GsonUtil;
import com.anchnet.util.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AuthFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String path = exchange.getRequest().getURI().getPath();
        /**
         *  不对 /login 和 /register 进行权限认证
         */
        if (!path.equals("/login") && !path.equals("/register")) {
            String token = exchange.getRequest().getHeaders().getFirst("token");
            if (StrUtil.isBlank(token)) {
                log.info("auth failed");
                exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);

                String json = GsonUtil.toJson(Result.unauthorized());
                return exchange.getResponse().writeWith(Mono.just(exchange.getResponse().bufferFactory().wrap(json.getBytes())));
            }

            User user = getUserByToken(token);
            ServerHttpRequest.Builder builder = exchange.getRequest().mutate();
            builder.header("UserId", user.getId().toString());
            exchange.mutate().request(builder.build()).build();
        }

        return chain.filter(exchange);
    }

    private User getUserByToken(String token) {
        return TokenUtil.parseToken(token);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
