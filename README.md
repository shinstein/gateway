# gateway

#### 介绍
微服务网关之gateway 

| 微服务名称   | 地址                                  | 
|:--------|:------------------------------------| 
| gateway | https://gitee.com/shinstein/gateway | 
| order   | https://gitee.com/shinstein/order   | 
| user    | https://gitee.com/shinstein/user | 
| product | https://gitee.com/shinstein/product | 

#### 软件架构
spring-cloud-starter-gateway 3.1.8 + gradle + nacos 2021.1 + gson 2.10.1


#### 安装教程

# 1. docker 运行 nacos 
启动完成后 http://192.168.62.148:8848/nacos 页面访问 nacos ， 用户名: nacos   密码: nacos
```shell
docker run --name nacos -e MODE=standalone -p 8848:8848 -d nacos/nacos-server
```

# 2. docker 运行 mysql  
user项目和product项目
```shell
docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql:8.0.28
```

# 3. docker 运行 redis 
user项目整合
```shell
docker run --name redis -d -p 6379:6379 redis
```

# 4. docker 运行 rabbitmq 
启动完成后 http://192.168.62.148:15672 页面访问 rabbitmq ， 用户名: guest   密码: guest
order 项目发送消息，product项目消费消息
```shell
docker run -d --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management
```

# 5. docker 运行 es 
作为 skywalling 的数据存储端，启动成功后 访问 http://192.168.62.148:9200，本例未使用 es 和 kibana
```shell
docker run -d --name es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms64m -Xmx512m" elasticsearch:7.17.3
```

# 6. docker 启动 kibana
作为es可视化，启动成功后访问  访问 192.168.62.148:5601，本例未使用 es 和 kibana
```shell
docker run -d --name kibana -p 5601:5601 -e I18N_LOCALE=zh-CN -e ELASTICSEARCH_HOSTS=http://192.168.62.148:9200 kibana:7.17.3
```

# 7. docker 启动 skywalking  
本例未使用es 存储，使用默认的H2数据库，使用当前skywalking最新版本9.6.0

9.x版本一直没数据或启动失败是因为 9.0.0的 agent 有问题，换成 8.16.0的agent 就好了 ，所有服务使用同一个agent

**skywalking 启动后一定要在 skywalking-ui页面设置中将自动更新打开，否则会没数据**


```shell
#docker run --name skywalking --restart always -d -p 12800:12800 -p 11800:11800 -p 9412:9412 -v /data/skywalking/mappings:/skywalking/oap-libs/mappings -e SW_STORAGE=elasticsearch -e SW_STORAGE_ES_CLUSTER_NODES=192.168.62.148:9200  -e TZ=Asia/Shanghai apache/skywalking-oap-server:9.6.0

docker run --name skywalking --restart always -d -p 12800:12800 -p 11800:11800 -p 9412:9412 -e TZ=Asia/Shanghai apache/skywalking-oap-server:9.6.0
```


# 8. docker 启动 skywalking-ui
启动成功后访问 192.168.62.149:8088
9.x版本一直没数据或启动失败是因为 9.0.0的 agent 有问题，换成 8.16.0的agent 就好了 ，所有服务使用同一个agent

```shell
docker run --name skywalking-ui --restart always -d -p 8088:8080 -e SW_OAP_ADDRESS=http://192.168.62.149:12800 -e SW_ZIPKIN_ADDRESS=http://192.168.62.149:9412 -e TZ=Asia/Shanghai apache/skywalking-ui:9.6.0
```


# 9. idea vm 配置
skywalking.agent.service_name 根据不同服务按需填写
```shell
-javaagent:F:\javawork\cloud\skywalking-agent-8.16.0\skywalking-agent.jar -Dskywalking.agent.service_name=gateway -Dskywalking.collector.backend_service=192.168.62.149:11800
```

# 10. mysql
登录mysql 容器
```shell
docker exec -it mysql bash

export LANG=C.UTF-8
mysql -uroot -proot --default-character-set=utf8
```

mysql 语句见 scheme.sql 
