create database cloud;

create table `product` (
	id int(11) unsigned not null auto_increment primary key,
	name varchar(64) not null,
	price decimal(8,2) not null,
	stock int(11) unsigned
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

create table `user` (
	id int(11) unsigned not null auto_increment primary key,
	name varchar(64) not null
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

create table `product_order` (
	id int(11) unsigned not null auto_increment primary key,
	user_id int(11) unsigned not null,
	product_id int(11) unsigned not null,
	number int(11) unsigned not null
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;


insert into product (name, price, stock) values ('牙刷', 11.5, 100), ('牙膏', 23.5, 1000);
insert into user (name) values ('张三'), ('李四');
insert into product_order (user_id, product_id, number) values (1, 1, 2), (2, 2, 10);